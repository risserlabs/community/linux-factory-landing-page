import React from "react";
import { YStack, H1, H2, Paragraph, Text, XStack, Separator } from "ui";
import { Link } from "solito/link";
import { Copyright, Github, Gitlab } from "@tamagui/lucide-icons";
import { SimpleImage } from "ui/src/images";

import { withDefaultLayout } from "app/layouts/Default";
export const LandingPage = () => {
  return (
    <YStack jc="center" space="$8">
      <YStack
        bc="$backgroundFocus"
        width="100%"
        space={4}
        padding={30}
        top={0}
        left={0}
        right={0}
        jc="center"
        ai="center"
      >
        <H1>LINUX-FACTORY</H1>
        <Separator />
        <H2 textAlign="center">
          A framework used to create custom linux debian operating systems
        </H2>
      </YStack>

      <YStack ai="center" jc="center" p="$10">
        <Paragraph maxWidth="80%" textAlign="center">
          Linux-factory is a comprehensive tool for building custom Debian-based
          operating systems. It provides users with a range of features,
          including overlays, hooks, and a clear and organized file structure,
          to help them configure their operating system to their specifications.
        </Paragraph>
        <Separator />
        <Paragraph maxWidth="80%" textAlign="center">
          Overlays are one of the main features of Linux-factory. They are
          customizable, decoupled customizations that can be mixed and matched
          with other overlays or disabled altogether. Overlays are applied on
          top of the os directory during the build process, which means that the
          file structure inside of an overlay and the file structure of the os
          directory are identical.
        </Paragraph>
        <Separator />
        <Paragraph maxWidth="80%" textAlign="center">
          Linux-factory also supports hooks, which allow users to run custom
          code during the build process. Hooks can be implemented at different
          stages of the build process, including before and after the build,
          before and after the configuration, and before and after the
          preparation stage. This makes it possible for users to run custom
          scripts, add additional packages, or perform other customizations
          during the build process.
        </Paragraph>
        <Separator />
        <Paragraph maxWidth="80%" textAlign="center">
          The file structure of Linux-factory is organized and easy to navigate.
          Users can specify which repositories they want to include in their
          live system or live medium, and they can add files to be included in
          either of these as well. The framework also supports package lists,
          which allow users to specify which packages they want to include in
          the APT pool, which packages to install in the live system, and which
          packages to install in the installed system.
        </Paragraph>
        <Separator />
        <Paragraph maxWidth="80%" textAlign="center">
          Overall, Linux-factory is a powerful tool for building custom
          Debian-based operating systems. It provides users with a range of
          features to help them configure their operating system to their
          specifications, including overlays, hooks, and an organized file
          structure. With Linux-factory, users can create custom operating
          systems with ease, making it an ideal tool for developers, system
          administrators, and anyone else who needs a customized Linux
          environment.
        </Paragraph>
      </YStack>
      <YStack ai="center">
        <SimpleImage
          maxWidth={600}
          minHeight={400}
          src="https://raw.githubusercontent.com/clayrisser/linux-factory/main/assets/linux-factory.jpeg"
        />
      </YStack>
      <YStack bottom={0}>
        <YStack ai="center" bc="$background" padding={150}>
          <XStack ai="center" space>
            <YStack animation="bouncy" hoverStyle={{ scale: 1.1 }} space>
              <Link
                tag="a"
                rel="noopener noreferrer"
                href="https://github.com/clayrisser/linux-factory"
              >
                <Github size="$4" />
              </Link>
            </YStack>

            <Separator vertical={true} />
            <Separator vertical={true} />

            <YStack animation="bouncy" hoverStyle={{ scale: 1.1 }} space>
              <Link
                tag="a"
                rel="noopener noreferrer"
                href="https://gitlab.com/risserlabs/community/linux-factory"
              >
                <Gitlab size="$4" />
              </Link>
            </YStack>
          </XStack>
        </YStack>
        <YStack
          paddingLeft="10%"
          width="100%"
          jc="flex-end"
          bottom={20}
          space
          position="absolute"
        >
          <Separator borderWidth="$1" width="90%" />
          <XStack ai="center">
            <Copyright />
            <Text>RisserLabs</Text>
          </XStack>
        </YStack>
      </YStack>
    </YStack>
  );
};

export default withDefaultLayout(LandingPage);
