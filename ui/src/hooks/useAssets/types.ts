import type { ImageURISource } from 'react-native';

export type Asset = ImageURISource;
